    <?php require_once('web-interface/includes/session.php'); ?>
    
    <?php //confirm_logged_in(); ?>
    
                <?php
                    $page = filter_input(INPUT_GET, "page");
                    if(!$page){
                     $page = "NULL";   
                    }
                ?>
                
                <?php 
                    // if the user press the button from the side bar menu 
                    // the panel is loaded with the information called
                    
                    if(isset($_GET['dashboard'])){
                    
                       include("web-interface/dashboard-website-student/functions/dashboard.php");
                    }   
                    
                    if(isset($_GET['get-user-admin-list'])){

                        include("web-interface/dashboard-website-student/functions/get-user-admin-list.php");
                    }
                    
                    if(isset($_GET['get-lesson-per-user'])){

                        include("web-interface/dashboard-website-student/functions/get-lesson-per-user.php");
                    }
                    
                    if(isset($_GET['get-attend'])){

                        include("web-interface/dashboard-website-student/functions/get-attendance.php");
                    }
                    
                    if(isset($_GET['mark-attendance'])){

                        include("web-interface/dashboard-website-student/functions/mark-attendance.php");
                    }
                    
                    if(isset($_GET['get-lesson'])){

                        include("web-interface/dashboard-website-student/functions/get-lesson.php");
                    }
                    
                    if(isset($_GET['get-rooms'])){

                        include("web-interface/dashboard-website-student/functions/get-rooms.php");
                    }
                    
                    if(isset($_GET['get-timeslots'])){

                        include("web-interface/dashboard-website-student/functions/get-timeslots.php");
                    }
                    
                    if(isset($_GET['get-student'])){

                        include("web-interface/dashboard-website-student/functions/get-student.php");
                    }
                    
                    if(isset($_GET['get-class'])){

                        include("web-interface/dashboard-website-student/functions/get-class.php");
                    }
                    if(isset($_GET['get-enrol'])){

                        include("web-interface/dashboard-website-student/functions/get-enrol.php");
                    }
                    
                    if(isset($_GET['get-beacon'])){

                        include("web-interface/dashboard-website-student/functions/get-beacon.php");
                    }
                    
                    if(isset($_GET['student_attend_detail'])){

                        include("web-interface/dashboard-website-student/functions/student_attend_detail.php");
                    }
                    
                 ?>
            
    <?php 
    
        if(isset($_GET['add-beacon'])){
           include("web-interface/dashboard-website-student/forms/add-beacon.php");
        }
    
    
    ?>