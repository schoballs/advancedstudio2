<?php require_once('web-interface/includes/session.php'); ?>
<?php require_once('web-interface/dashboard-website/functions/functions.php'); ?>
<?php include 'header.php'; ?>
            <!-- Page Content -->
            <div id="page-content-wrapper">
                <div class="container-fluid xyz">
                    <div class="row rowTop">
                        <!--<div class="col-lg-12">-->
                        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                            
                            <?php
                                
                                if($session_login == "YES"){
                                    if($_SESSION['role_id'] == "student"){
                                        header('Location: body-website-student.php?dashboard');
                                    }else{
                                         header('Location: body-website.php?dashboard');
                                    }
                                  
                                }else{
                                    
                                    $_SESSION["login"] = "NO";
                                    
                                    session_destroy();
                                    
                                    include 'change-user-type.php';
                                }
                               //echo "<pre>" .print_r($_SESSION, TRUE). "</pre>";
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->    
        </div>
    </div>
    <?php include 'footer.php'; ?>  

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="web-interface/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="web-interface/assets/js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="web-interface/assets/js/ie10-viewport-bug-workaround.js"></script>
    <!-- Jquery version for the side bar menu-->
    <script src="web-interface/js/jquery-1.11.3.js"></script>
    <!-- Javascript code for the sidebar menu-->
    <script src="web-interface/js/simple-sidebar.js"></script>
    
  </body>
</html>