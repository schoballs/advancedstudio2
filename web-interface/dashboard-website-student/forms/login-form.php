<?php 
    require_once('web-interface/includes/session.php');
?>     
    <!-- LOGIN FORM -->    

        <div class="container col-md-12 margin-top-med ">
            <div class="container col-lg-7 col-md-7 col-sm-5 center-page" >
                <form action="web-interface/dashboard-website-student/functions/login-check-user.php" id="login-action" method="post">
                    <div id="form-group">
                        <fieldset>
                            <legend>Teacher Login</legend>
                            <div id="form-group">
                                <input type="text" class="form-control" id="username" name="username" placeholder="Email" autofocus value=""/><br>
                            </div>
                            <div id="form-group">
                                <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password"/><br>
                            </div>
                            <div id="form-group">
                                <input type="submit" class="btn btn-primary" id="submit" name="submit" value="Login">
                            </div>
                        </fieldset>
                    </div>
                </form>
                <ul class="nav navbar-nav navbar-left"><li><a href="index.php"><span class="glyphicon glyphicon-log-in"></span> Change User Type</a></li></ul>
            </div>
        </div> <!-- end of Login Form-->