<?php  
    require_once('web-interface/includes/session.php');
    include("web-interface/connection/config.php");
    require_once("web-interface/dashboard-website-student/functions/functions.php");
?>




<?php
    
    if(isset($_GET['student_attend_detail'])){
    
        $student_id = $_GET['student_id'];
        
        $class_id = $_GET['class_id'];
        
        $get_student = "select student_prefname from student where student_id = '$student_id' ";
        $fetch_student = pg_query($dbconn, $get_student);
        
        $student_selected =  pg_fetch_array($fetch_student);
        
        $student_name = $student_selected['student_prefname'];
?>



<h2 class="sub-header">Attendance - <span style="font-weight: 100; letter-spacing: 3px;"><?php echo $student_name ; ?></span></h2>            

<div class="table-responsive">
    <table class="table table-striped">
        <thead> 
            <tr align="center" >
                <th>Lesson Date</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
        <?php
        
            
            $fetch_attendance_detail = get_attendance_detail($student_id, $class_id);
    
            $index = 0;

            while($attendance_detail_row = pg_fetch_array($fetch_attendance_detail)){
                
                $lesson_date = $attendance_detail_row['lesson_date'];
                $student_name = $attendance_detail_row['student_prefname'];
                $lesson_id = $attendance_detail_row['lesson_id'];
                
                $student_attended = check_attendance_student_lesson($student_id, $lesson_id);
                $fetch_student_attended = pg_fetch_array($student_attended);
                
                if($fetch_student_attended['attend_id'] != ''){
                    $status = 'attended';
                    $style_row = 'success';
                }else{
                    $status = 'absent';
                    $style_row = 'danger';
                }

                $index++;
        ?>
            <tr class="<?php echo $style_row; ?>" >
                <td><?php echo $lesson_date; ?></td>
                <td><?php echo $status; ?></td>
                
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>    

    <?php }else{
        
        echo 'Access DENIED!!!';
        
        
    } ?>