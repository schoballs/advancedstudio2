<?php 
    require_once('web-interface/includes/session.php');
    include("web-interface/connection/config.php");
    require_once("web-interface/dashboard-website-student/functions/functions.php");
?>
<h2 class="sub-header">Beacons List</h2>            

<div class="table-responsive">
    <table class="table table-striped">
        <thead> 
            <tr>
                <th>beacon_id</th>
                <th>room_id</th>
                <th>beacon_uuid</th>
                <th>beacon_serial_number</th>
                <th>beacon_region</th>
                <th>beacon_major</th>
                <th>beacon_minor</th>
            </tr>
        </thead>
        <tbody>

        <?php

            $fetch_beacon = get_all_beacon();

            $index = 0;

            while($beacon_row = pg_fetch_array($fetch_beacon)){

                $beacon_id = $beacon_row['beacon_id'];   
                $beacon_room_id = $beacon_row['room_id'];   
                $beacon_uuid = $beacon_row['beacon_uuid']; 
                $beacon_serial_number = $beacon_row['beacon_serial_number']; 
                $beacon_region = $beacon_row['beacon_region']; 
                $beacon_major = $beacon_row['beacon_major']; 
                $beacon_minor = $beacon_row['beacon_minor']; 

                $index++;
        ?>
            <tr align="center">
                <td><?php echo $beacon_id; ?></td>
                <td><?php echo $beacon_room_id; ?></td>
                <td><?php echo $beacon_uuid; ?></td> 
                <td><?php echo $beacon_serial_number; ?></td> 
                <td><?php echo $beacon_region; ?></td>
                <td><?php echo $beacon_major; ?></td>
                <td><?php echo $beacon_minor; ?></td>
            </tr>

        <?php } ?>
            
        </tbody>
    </table>
</div>