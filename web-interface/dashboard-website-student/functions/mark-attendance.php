<?php  
    require_once('web-interface/includes/session.php');
    include("web-interface/connection/config.php");
    require_once("web-interface/dashboard-website-student/functions/functions.php");
?>

<?php 
    //echo $user_id;
    
    //echo "Teacher: ".$_SESSION['user_fname']." ".$_SESSION['user_lname'];
    
    $values = $_SESSION['array-checkbox'];
    
    echo $values[0];
?>

<form method="post" >
    <select type="" class="form-control" id="lesson_date_picker" name="lesson_date_picker" placeholder="Room" hint="Lesson Date">
        <option value="" ></option>
        <?php  $lesson_date_set = get_lesson_date($_SESSION['user_id']); 
        
            while($lesson_date_row = pg_fetch_array($lesson_date_set)){
        ?>        
           <option value="<?php echo $lesson_date_row['lesson_date'];?>"> <?php echo $lesson_date_row['lesson_date']; ?> </option>    
        <?php        
            }
        ?>
    </select>
    <input class="btn btn-aitendance" type="submit" value="Change Date">
</form>

<?php 
    if(!isset($_POST['lesson_date_picker'])){
       $today = date('Y-m-d');
       
    }else{
        $today = $_POST['lesson_date_picker'];
    }

?>

<h2 class="sub-header">Mark Attendance</h2>    

<?php    

    $fetch_lesson_per_user = get_lesson_per_user_timeslot($_SESSION['user_id'], $today);
    
     $index = 0;
     
    while($lesson_per_user_row = pg_fetch_array($fetch_lesson_per_user)){

        $timetable_lesson_id = $lesson_per_user_row['lesson_id'];   
        $timetable_lesson_class_id = $lesson_per_user_row['class_id']; 
        $timetable_lesson_date = $lesson_per_user_row['lesson_date']; 
        $timetable_lesson_timeslot = $lesson_per_user_row['lesson_timeslot']; 
        $timetable_timeslots_dayofweek = $lesson_per_user_row['dayofweek'];
        $timetable_start_time = $lesson_per_user_row['start_time']; 
        $timetable_end_time = $lesson_per_user_row['end_time'];
        $timetable_room_name = $lesson_per_user_row['name'];
        
        $originalDateFormat = $timetable_lesson_date;
        $newDateFormat = date("d-m-Y", strtotime($originalDateFormat));
    
?>
     <div class=" panel panel-success table-responsive">
         
        <div class="panel-heading "> 
            <span><?php echo "Class: ".$timetable_lesson_class_id; ?></span>
            <span><?php echo "Lesson: ".$timetable_lesson_id; ?></span>
            <span><?php echo "Room: ".$timetable_room_name; ?></span>
            <span class="span-float-right"><?php echo $timetable_start_time." - ".$timetable_end_time." - Date: ".$newDateFormat; ?></span>
        </div>
        
        <div class="panel-body ">
            
            <form method="post" action="web-interface/dashboard-website-student/functions/check-attendance.php">
                
                <table id="myTable" class="tablesorter table">
                    <thead>
                        <tr>
                            <th>Attend</th>
                            <th>Student Id</th>
                            <th>Student Name</th>
                            <th>Student Pref Name</th>
                            <th>Course</th>
                            <th>Status</th>
                            <th>Attend Detail</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $fetch_student_per_lesson = get_lesson_per_student($_SESSION['user_id'], $timetable_lesson_id);

                            $index = 0;

                            while($student_per_lesson_row = pg_fetch_array($fetch_student_per_lesson)){

                                $student_id = $student_per_lesson_row['student_id'];   
                                $student_full_name = $student_per_lesson_row['student_fname']." ".$student_per_lesson_row['student_lname']; 
                                $student_prfname = $student_per_lesson_row['student_prefname'];
                                $student_stream = $student_per_lesson_row['stream'];
                                $student_lesson = $student_per_lesson_row['lesson_id'];
                                $student_class = $student_per_lesson_row['class_id'];
                        ?>
                        <?php

                            /* getting any record of attendance by the row student and lesson -- 
                            in case there is one record, it means that the student attended that specific class */

                            $fetch_attendance_per_student_lesson = get_attendance($student_id, $student_lesson);
                            $attendance_per_student_lesson_row = pg_fetch_array($fetch_attendance_per_student_lesson);

                            $student_attend = $attendance_per_student_lesson_row['attend_id']; 
                        ?>
                        
                        <tr class="<?php if ($student_attend != null){ echo 'success'; }else{ echo 'danger';}  ?>">
                            <td>
                                <input style="font-size: 100px;" type="checkbox" name="attendance_array[<?php echo $index; ?>][student_id]" id="attendance" value="<?php echo $student_id; ?>"></input> 
                                <input type="hidden" name="attendance_array[<?php echo $index; ?>][student_lesson]" id="attendance" value="<?php echo $student_lesson; ?>"></input>
                                <input type="hidden" name="attendance_array[<?php echo $index; ?>][attend_id]" id="attendance" value="<?php echo $student_attend; ?>"></input>
                            </td>
                            <td><?php echo $student_id; ?></td>
                            <td><?php echo $student_full_name; ?></td>
                            <td><?php echo $student_prfname; ?></td>
                            <td><?php echo $student_stream; ?></td>
                            <td> <?php if ($student_attend != null){ echo "<span class='success'>Attended</span>";}else{ echo "<span class='danger'>Absent</span>";}  ?> </td>
                            <td><a class="btn btn-aitendance" href="body-website.php?student_attend_detail&student_id=<?php echo $student_id; ?>&class_id=<?php echo $student_class; ?>">Details »</a>
<!--                                <form method="POST">
                                    <input type="submit" class="btn btn-aitendance" name="attend_detail" value="Detail"></input>
                                    <input type="hidden" name="student_id" value="<?php //echo $student_id; ?>"></input>
                                    <input type="hidden" name="class_id" value="<?php //echo $class_id; ?>"></input>
                                </form>-->
                        </tr>
                    <?php 
                            $index++;    
                            } 
                        ?>
                    </tbody>
                </table>
                <input type="submit" class="btn btn-success" name="mark_attendance" value="Mark Attendance"></input>
                <input type="submit" onclick="return confirm('Are you sure you want to continue')" class="btn btn-danger" name="mark_absence" value="Mark Absence"></input>
            </form>
        </div> 
    </div>
   

    <?php }?>