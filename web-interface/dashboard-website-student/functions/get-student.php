<?php require_once('web-interface/includes/session.php'); ?>

<h2 class="sub-header">Student List</h2> 

<div class="table-responsive">
    
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Student Id</th>
                <th>student_fname </th>
                <th>student_lname </th>
                <th>student_prefname </th>
                <th>student_enroldate </th>
                <th>student_enddate </th>
                <th>os </th>
                <th>stream </th>
                <th>title </th>
                <th>gender </th>
                <th>dob </th>
                <th>photo </th>
                <th>language </th>
                <th>pp_number </th>
                <th>pp_country </th>
                <th>visa_type </th>
                <th>visa_number </th>
                <th>healthcarenum </th>
                <th>status </th>
                <th>coe_var</th>
                <th>changed_finance</th>
                <th>suspended</th>
                <th>parttime</th>
                <th>chessn</th>
                <th>debt_recover</th>
                <th>school_level</th>
                <th>school_level_year </th>
                <th>labour_status</th>
                <th>education_old </th>
                <th>at_school</th>
                <th>english_prof</th>
                <th>indigenous</th>
                <th>disability</th>
                <th>study_reason</th>
                <th>unique_student_identifier</th>

            </tr>
        </thead>
        <tbody>

        <?php
        include("web-interface/connection/config.php");

            $get_student = "select * from student";
            $fetch_student = pg_query($dbconn, $get_student);

            $index = 0;

            while($student_row = pg_fetch_array($fetch_student)){

                $student_id = $student_row['student_id'];   
                $student_fname = $student_row['student_fname']; 
                $student_lname = $student_row['student_lname']; 
                $student_prefname = $student_row['student_prefname']; 
                $student_enroldate = $student_row['student_enroldate'];
                $student_enddate = $student_row['student_enddate'];
                $student_os = $student_row['os'];
                $student_stream = $student_row['stream'];
                $student_title = $student_row['title'];
                $student_gender = $student_row['gender'];
                $student_dob = $student_row['dob'];
                $student_photo = $student_row['photo'];
                $student_language = $student_row['language'];
                $student_pp_number = $student_row['pp_number'];
                $student_pp_country = $student_row['pp_country'];
                $student_visa_type = $student_row['visa_type'];
                $student_visa_number = $student_row['visa_number'];
                $student_healthcarenum = $student_row['healthcarenum'];
                $student_status = $student_row['status'];
                $student_coe_var = $student_row['coe_var'];
                $student_changed_finance = $student_row['changed_finance'];
                $student_suspended = $student_row['suspended'];
                $student_parttime = $student_row['parttime'];
                $student_chessn = $student_row['chessn'];
                $student_debt_recover = $student_row['debt_recover'];
                $student_school_level = $student_row['school_level'];
                $student_school_level_year = $student_row['school_level_year'];
                $student_labour_status = $student_row['labour_status'];
                $student_education_old = $student_row['education_old'];
                $student_at_school = $student_row['at_school'];
                $student_english_prof = $student_row['english_prof'];
                $student_indigenous = $student_row['indigenous'];
                $student_disability = $student_row['disability'];
                $student_education = $student_row['education'];
                $student_study_reason = $student_row['study_reason'];
                $student_unique_student_identifier = $student_row['unique_student_identifier'];

                $index++;

        ?>

            <tr align="center">
                <td><?php echo $student_id; ?></td>
                <td><?php echo $student_fname; ?></td>
                <td><?php echo $student_lname; ?></td>
                <td><?php echo $student_prefname; ?></td>
                <td><?php echo $student_enroldate; ?></td>
                <td><?php echo $student_enddate; ?></td>
                <td><?php echo $student_stream; ?></td>
                <td><?php echo $student_title; ?></td>
                <td><?php echo $student_gender; ?></td>
                <td><?php echo $student_dob; ?></td>
                <td><?php echo $student_photo; ?></td>
                <td><?php echo $student_language; ?></td>
                <td><?php echo $student_pp_number; ?></td>
                <td><?php echo $student_pp_country; ?></td>
                <td><?php echo $student_visa_type; ?></td>
                <td><?php echo $student_visa_number; ?></td>
                <td><?php echo $student_healthcarenum; ?></td>
                <td><?php echo $student_status; ?></td>
                <td><?php echo $student_coe_var; ?></td>
                <td><?php echo $student_changed_finance; ?></td>
                <td><?php echo $student_suspended; ?></td>
                <td><?php echo $student_parttime; ?></td>
                <td><?php echo $student_chessn; ?></td>
                <td><?php echo $student_debt_recover; ?></td>
                <td><?php echo $student_school_level; ?></td>
                <td><?php echo $student_school_level_year; ?></td>
                <td><?php echo $student_labour_status; ?></td>
                <td><?php echo $student_education_old; ?></td>
                <td><?php echo $student_at_school; ?></td>
                <td><?php echo $student_english_prof; ?></td>
                <td><?php echo $student_indigenous; ?></td>
                <td><?php echo $student_disability; ?></td>
                <td><?php echo $student_education; ?></td>
                <td><?php echo $student_study_reason; ?></td>
                <td><?php echo $student_unique_student_identifier; ?></td>
                <td><a href="admin-page.php?edit-client=<?php echo $user_username; ?>">Edit</a></td>
            </tr>

            <?php } ?>

        </tbody>
    </table>
</div>    