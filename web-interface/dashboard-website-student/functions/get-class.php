<?php 
    require_once('web-interface/includes/session.php');
    include("web-interface/connection/config.php");
    require_once("web-interface/dashboard-website-student/functions/functions.php");
?>
<h2 class="sub-header">Class List</h2>            
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>class_id</th>
                <th>start_date</th>
                <th>course_id</th>
                <th>end_date</th>
                <th>daysofweek</th>
                <th>hoursperday</th>
                <th>semester_id</th>
                <th>class_teacher</th>
            </tr>
        </thead>
        <tbody>

        <?php
            $get_class = "select * from class";
            $fetch_class = pg_query($dbconn, $get_class);

            $index = 0;

            while($class_row = pg_fetch_array($fetch_class)){

                $class_id = $class_row['class_id'];   
                $class_start_date = $class_row['start_date'];
                $class_course_id = $class_row['course_id'];
                $class_end_date = $class_row['end_date'];
                $class_daysofweek = $class_row['daysofweek'];
                $class_hoursperday = $class_row['hoursperday'];
                $class_semester_id = $class_row['semester_id'];
                $class_class_teacher = $class_row['class_teacher'];

                $index++;
        ?>
            <tr align="center">
                <td><?php echo $class_id; ?></td>
                <td><?php echo $class_start_date; ?></td>
                <td><?php echo $class_course_id; ?></td>
                <td><?php echo $class_end_date; ?></td> 
                <td><?php echo $class_daysofweek; ?></td> 
                <td><?php echo $class_hoursperday; ?></td>
                <td><?php echo $class_semester_id; ?></td>
                <td><?php echo $class_class_teacher; ?></td>
            </tr>
        <?php } ?>
        
        </tbody>
    </table>
</div>