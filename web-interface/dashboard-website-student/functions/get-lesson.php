<?php  require_once('web-interface/includes/session.php'); ?>

<h2 class="sub-header">Lesson List</h2>            

<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Lesson Id</th>
                <th>Class Id</th>
                <th>Lesson No</th>
                <th>Lesson Date</th>
                <th>Lesson Hours</th>
                <th>Lesson Timeslot</th>
                <th>Lesson Room</th>
                <th>Lesson Teachers</th>
                <th>Lesson Assistant</th>
            </tr>
        </thead>
        <tbody>

        <?php

        include("web-interface/connection/config.php");

                $get_lesson = "select * from lesson ORDER BY lesson_date DESC";
                $fetch_lesson = pg_query($dbconn, $get_lesson);

                $index = 0;

                while($lesson_row = pg_fetch_array($fetch_lesson)){

                    $lesson_id = $lesson_row['lesson_id'];   
                    $lesson_class_id = $lesson_row['class_id']; 
                    $lesson_no = $lesson_row['lesson_no']; 
                    $lesson_date = $lesson_row['lesson_date']; 
                    $lesson_hours = $lesson_row['lesson_hours'];
                    $lesson_timeslot = $lesson_row['lesson_timeslot']; 
                    $lesson_room = $lesson_row['lesson_room']; 
                    $lesson_teachers = $lesson_row['lesson_teacher'];
                    $lesson_asssitant = $lesson_row['lesson_assistant']; 
                    
                    $index++;

            ?>

            <tr align="center">
                <td><?php echo $lesson_id; ?></td>
                <td><?php echo $lesson_class_id; ?></td>
                <td><?php echo $lesson_no; ?></td>
                <td><?php echo $lesson_date; ?></td> 
                <td><?php echo $lesson_hours; ?></td> 
                <td><?php echo $lesson_timeslot; ?></td>
                <td><?php echo $lesson_room; ?></td>
                <td><?php echo $lesson_teachers; ?></td>
                <td><?php echo $lesson_asssitant; ?></td>
            </tr>

            <?php } ?>
        
        </tbody>
    </table>
</div>
            