<?php  
    require_once('web-interface/includes/session.php');
    include("web-interface/connection/config.php");
    require_once("web-interface/dashboard-website-student/functions/functions.php");
    
    //$today = '2016-02-09';
    
    $today = date('Y-m-d');
?>

<div class="row">
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-pink">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-check-square-o fa-4x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"> <?php echo number_of_lessons($_SESSION['user_id'], $today); ?></div>
                        <div>Classes Today</div>
                        
                    </div>
                </div>
            </div>
            <a href="body-website.php?mark-attendance">
                <div class="panel-footer">
                    <span class="pull-left">Mark Attendance</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-calendar fa-4x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"> <?php echo number_of_lessons($_SESSION['user_id'], $today); ?></div>
                        <div>Classes Today</div>
                        
                    </div>
                </div>
            </div>
            <a href="body-website.php?timetable">
                <div class="panel-footer">
                    <span class="pull-left">Timetable</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
</div>

