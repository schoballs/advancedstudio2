<?php require_once('web-interface/includes/session.php'); ?>

<h2 class="sub-header">Attendance List</h2>            

<div class="table-responsive">
    <table class="table table-striped">
        <thead> 
            <tr>
                <th>Attendance Id</th>
                <th>Student Id</th>
                <th>Lesson Id</th>
                <th>Hours Attend</th>
                <th>Late</th>
            </tr>
        </thead>
        <tbody>
        <?php

        include("web-interface/connection/config.php");

            $get_attend = "select * from attend";
            $fetch_attend = pg_query($dbconn, $get_attend);

            $index = 0;

            while($attend_row = pg_fetch_array($fetch_attend)){

                $attend_id = $attend_row['attend_id'];   
                $attend_student_id = $attend_row['student_id']; 
                $attend_lesson_id = $attend_row['lesson_id']; 
                $attend_hours = $attend_row['hours_attend']; 
                $attend_late = $attend_row['late']; 

                $index++;
        ?>
            <tr align="center">
                <td><?php echo $attend_id; ?></td>
                <td><?php echo $attend_student_id; ?></td>
                <td><?php echo $attend_lesson_id; ?></td>
                <td><?php echo $attend_hours; ?></td> 
                <td><?php echo $attend_late; ?></td> 
                <td><a href="admin-page.php?edit-client=<?php echo $user_username; ?>">Edit</a></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>    