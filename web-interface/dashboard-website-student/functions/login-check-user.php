<?php 
    require_once('../../includes/session.php');
?>
<?php
    /*
    ASSUMPTIONS FOR THE ROLES:
     * ADMIN - 1
     * STUDENT - Student
     * TEACHER - 3
     * HEAD TEACHER - 4
     * SYSTEM ADMIN - 5

    */
    require_once("../../connection/config.php");
    require_once("../functions/functions.php");
    //require_once("../includes/passwordLib.php");
    //return false if the validation fails.
    
    
    
    if(!$submit = pg_escape_string(filter_input(INPUT_POST,'submit'))){
        //if this page is called accidentally by another page without a submit button
        $_SESSION['message_danger'] ="validation of login fail ";
    }else{
        
        $username = pg_escape_string(filter_input(INPUT_POST,'username'));
        $password = pg_escape_string(filter_input(INPUT_POST,'password'));
        
        //if $username and $password dont pass the validations (because they dont have value and they are not null)
        if(!$username  || !$password ){
            if(!$username){
                $_SESSION['message_danger'] = " User name cannot be empty " ;
            }else{
                if(!$password){
                    $_SESSION['message_danger'] = "password cannot be empty " ;
                }
            }
            //go back to the previous page
            header("Location: ../../../body-website-student.php");
        }else{
           //get the user's information
            $result_student_ua = get_student_ua_info($username);
            // count how many rows the result set returned
            $result_row_student_ua_count = pg_num_rows($result_student_ua);
            
            //if it returned one row then the user exist 
            if($result_row_student_ua_count == 1){
                
                
                //check the password.
                //get the user's data
                $student_ua_row = pg_fetch_assoc($result_student_ua);
                //validate password using php function
                $student_id = $student_ua_row['student_id'];
                 // get student's data
                $result_student = get_student_info($student_id);
                $student_row = pg_fetch_assoc($result_student);
                                
                if($password == $student_ua_row['student_ua_password']){
                    $_SESSION['message_success'] = "Login successful";
                    $_SESSION['username'] = $student_ua_row['student_ua_email'];
                    $_SESSION['login'] = "YES";
                    $_SESSION['role_id'] = 'student';
                    $_SESSION['user_id'] = $student_ua_row['student_id'];
                    $_SESSION['user_fname'] = $student_row['student_fname'];
                    $_SESSION['user_lname'] = $student_row['student_lname'];
                    
                    //$GLOBALS['user-loggedin'] = $user_row['username'];
                    
                    //return to the index.php webpage, and indicate in the url that the user is logged in.
                    header("Location:../../../body-website-student.php?dashboard&login=".urlencode("YES"));
                }else{
                    //login fail
                    $_SESSION['message_danger'] = " Password or user are incorrect " ;
                    $_SESSION['login'] = "NO";
                    header("Location: ../../../body-website-student.php?login-fail-wrong-password");
                }
            }else{
                //user doesn't exists
                $_SESSION['message_danger'] = "Password or user are incorrect";
                //go back to the previous page
                header("Location: ../../../body-website-student.php?this-is-user-problem");
            }
        }
    }

    //release the result sets
    if(isset($result))
        pg_free_result($result);

?>