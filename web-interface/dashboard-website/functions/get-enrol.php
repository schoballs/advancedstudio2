<?php 
    require_once('web-interface/includes/session.php');
?>

<h2 class="sub-header">Enrol List</h2>            
<div class="table-responsive">
    <table class="table table-striped">
        <thead> 
            <tr>
                <th>Enrol Id</th>
                <th>class_id </th>
                <th>student_id </th>
                <th>commenced</th>
                <th>enrol_date</th>
                <th>end_date</th>
                <th>result</th>
                <th>comment</th>
                <th>final_date</th>
                <th>qualification</th>
            </tr>
        </thead>
        
        <tbody>
        
        <?php
        include("web-interface/connection/config.php");

            $get_enrol = "select * from enrol";
            $fetch_enrol = pg_query($dbconn, $get_enrol);

            $index = 0;

            while($enrol_row = pg_fetch_array($fetch_enrol)){

                $enrol_id = $enrol_row['enrol_id'];   
                $enrol_class_id = $enrol_row['class_id'];
                $enrol_student_id = $enrol_row['student_id'];
                $enrol_commenced = $enrol_row['commenced'];
                $enrol_enrol_date = $enrol_row['enrol_date'];
                $enrol_end_date = $enrol_row['end_date'];
                $enrol_result = $enrol_row['result'];
                $enrol_comment = $enrol_row['comment'];
                $enrol_final_date = $enrol_row['final_date'];
                $enrol_qualification = $enrol_row['qualification'];

                $index++;
        ?>

            <tr align="center">
                <td><?php echo $enrol_id; ?></td>
                <td><?php echo $enrol_class_id; ?></td>
                <td><?php echo $enrol_student_id; ?></td>
                <td><?php echo $enrol_commenced; ?></td> 
                <td><?php echo $enrol_enrol_date; ?></td> 
                <td><?php echo $enrol_end_date; ?></td>
                <td><?php echo $enrol_result; ?></td>
                <td><?php echo $enrol_comment; ?></td>
                <td><?php echo $enrol_final_date; ?></td>
                <td><?php echo $enrol_qualification; ?></td>
                
                <td><a href="admin-page.php?edit-client=<?php echo $user_username; ?>">Edit</a></td>
            </tr>

        <?php } ?>

        </tbody>
    </table>
</div>