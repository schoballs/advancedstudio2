<?php  
    require_once('web-interface/includes/session.php');
    include("web-interface/connection/config.php");
    require_once("web-interface/dashboard-website/functions/functions.php");
?>

<form method="post" >
    <select type="" class="form-control" id="lesson_date_picker" name="lesson_date_picker" placeholder="Room" hint="Lesson Date">
        <option value="" ></option>
        <?php  $lesson_date_set = get_lesson_date($_SESSION['user_id']); 
        
            while($lesson_date_row = pg_fetch_array($lesson_date_set)){
        ?>        
           <option value="<?php echo $lesson_date_row['lesson_date'];?>"> <?php echo $lesson_date_row['lesson_date']; ?> </option>    
        <?php        
            }
        ?>
    </select>
    <input class="btn btn-aitendance" type="submit" value="Change Date">
</form>

<?php 
    if(!isset($_POST['lesson_date_picker'])){
       $today = date('Y-m-d');
       
       
    }else{
        $today = $_POST['lesson_date_picker'];
        
    }
    
?>

<h2 class="sub-header">Timetable</h2>

    <div class=" panel panel-success table-responsive">
         
        <div class="panel-heading "> 
            <span><?php echo "Timetable <strong>".$_SESSION['user_fname']." ".$_SESSION['user_lname']."</strong>"; ?></span>
            <span class="span-float-right"><i class="fa fa-calendar fa-1x"></i><?php echo " <strong>".$today; ?></strong></span>
        </div>
        
        <div class="panel-body ">    
    
    <table class="table table-striped">
        
        <thead>
            <tr>
                <th>Lesson Id</th>
                <th>Class</th>
                <th>Time Slot</th>
                <th>Room</th>
                <th>Day of Week</th>
                <th>Start Time</th>
                <th>End Time</th>
            </tr>
        </thead>
        
        <tbody>
            <?php
               
                $fetch_timetable_per_user = get_lesson_per_user_timeslot($_SESSION['user_id'], $today);

                $index = 0;

                while($timetable_user_row = pg_fetch_array($fetch_timetable_per_user)){

                    $user_full_name = $_SESSION['user_fname']." ".$_SESSION['user_lname'];    
                    $lesson_id = $timetable_user_row['lesson_id'];   
                    $lesson_class_id = $timetable_user_row['course_id'];
                    $lesson_date = $timetable_user_row['lesson_date']; 
                    $lesson_timeslot = $timetable_user_row['lesson_timeslot']; 
                    $timeslots_dayofweek = $timetable_user_row['dayofweek'];
                    $start_time = $timetable_user_row['start_time']; 
                    $end_time = $timetable_user_row['end_time'];
                    $room_name = $timetable_user_row['name'];


                    $index++;
            ?>

            <tr align="center">
                <td><?php echo $lesson_id; ?></td>
                <td><?php echo $lesson_class_id; ?></td>
                <td><?php echo $lesson_timeslot; ?></td> 
                <td><?php echo $room_name; ?></td>
                <td><?php echo $timeslots_dayofweek; ?></td>
                <td><?php echo $start_time; ?></td>
                <td><?php echo $end_time; ?></td>
            </tr>

        <?php } ?>

        </tbody>
    </table>
</div>
<?php 
//    }
?>