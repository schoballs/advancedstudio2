<?php require_once('web-interface/includes/session.php'); ?>

<h2 class="sub-header">User List</h2>            

<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Username</th>
                <th>Last Name</th>
                <th>First Name</th>
                <th>Header</th>
            </tr>
        </thead>
        <tbody>
        <?php

        include("web-interface/connection/config.php");

            $get_user = "select * from users";
            $fetch_user = pg_query($dbconn, $get_user);

            $index = 0;

            while($user_row = pg_fetch_array($fetch_user)){

                $user_username = $user_row['username'];   
                $user_password = $user_row['password']; 
                $user_active = $user_row['active_yn']; 
                $user_firstname = $user_row['user_fname']; 
                $user_lastname = $user_row['user_lname']; 
                $user_role = $user_row['role_cd'];
                $user_id = $user_row['user_id'];

                $index++;
        ?>
            <tr align="center">
                <td><?php echo $index; ?></td>
                <td><?php echo $user_username; ?></td>
                <td><?php echo $user_lastname; ?></td>
                <td><?php echo $user_firstname; ?></td> 
                <td><?php echo $user_role; ?></td> 
                <td><a href="admin-page.php?edit-client=<?php echo $user_username; ?>">Edit</a></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>      

            