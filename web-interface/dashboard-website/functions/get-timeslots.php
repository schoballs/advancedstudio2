<?php require_once('web-interface/includes/session.php'); ?>

<h2 class="sub-header">Timeslots List</h2> 

<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Slot Id</th>
                <th>Day Of The Week</th>
                <th>Name</th>
                <th>Start Time</th>
                <th>End Time</th>
                <th>End Time</th>
            </tr>
        </thead>
        <tbody>

            <?php

            include("web-interface/connection/config.php");

                $get_timeslots = "select * from timeslots";
                $fetch_timeslots = pg_query($dbconn, $get_timeslots);

                $index = 0;

                while($timeslots_row = pg_fetch_array($fetch_timeslots)){

                $slot_id = $timeslots_row['slot_id'];   
                $slot_dayofweek = $timeslots_row['dayofweek']; 
                $slot_name = $timeslots_row['name']; 
                $slot_start_time = $timeslots_row['start_time']; 
                $slot_end_time = $timeslots_row['end_time']; 
                $slot_sector_id = $timeslots_row['sector_id']; 

                $index++;

            ?>

            <tr align="center">
                <td><?php echo $slot_id; ?></td>
                <td><?php echo $slot_dayofweek; ?></td>
                <td><?php echo $slot_name; ?></td>
                <td><?php echo $slot_start_time; ?></td> 
                <td><?php echo $slot_end_time; ?></td> 
                <td><?php echo $slot_sector_id; ?></td> 
                <td><a href="admin-page.php?edit-client=<?php echo $user_username; ?>">Edit</a></td>
            </tr>
        
            <?php } ?>

        </tbody>
    </table>
</div>