    
<?php

    function confirm_query($result_set) {
	    if (!$result_set) {
		    die("Database query failed.");
    	    }
   }
   
   /* SHOW MESSAGES INSIDE OF THE SESSIONS messages of Success and Danger(for errors)*/
   function message(){
            if($_SESSION["message_success"] != null){
                $output = "<div class='alert alert-success'><Strong>Success! </strong>";
                $output .= htmlentities($_SESSION["message_success"]);
                $output .= "</div>";
                
                $_SESSION["message_success"] = null;
                
            return $output;
            }
            elseif($_SESSION["message_danger"] != null){
                $output = "<div class='alert alert-danger'><Strong>Got it wrong!</strong>";
                $output .= htmlentities($_SESSION["message_danger"]);
                $output .= "</div>";
                
                $_SESSION["message_danger"] = null;
                
                return $output;
            }
        }
        
      
        
    /* Get all the rooms */    
    function get_rooms(){
        global $dbconn;

        $query = "SELECT * FROM rooms";

        $room_set = pg_query($dbconn, $query);
        confirm_query($room_set);

        return $room_set;

    }
        
    /* Get Beacons Info by beacon_uuid */    
    function get_beacon_info($beacon_uuid){
        global $dbconn;

        $query = "SELECT * FROM beacon ";
        $query .= "WHERE beacon_uuid = '$beacon_uuid' ";

        $result = pg_query($dbconn, $query);
        confirm_query($result);

        return $result;
    }
    
    /* Register beacon in the database */
    function register_beacon($room_id, $beacon_serial_nu, $beacon_uuid, $beacon_region, $beacon_major, $beacon_minor){
        global $dbconn;
        
        $query  = "INSERT INTO beacon(room_id, beacon_serial_number, beacon_uuid, beacon_region, beacon_major, beacon_minor) ";
        $query .= "VALUES ('".$room_id."', '".$beacon_serial_nu."', '".$beacon_uuid."', '".$beacon_region."', '".$beacon_major."', '".$beacon_minor."') ";
        
        $result_set = pg_query($dbconn, $query);
        confirm_query($result_set);
        return $result_set;
    }
    
    
    /* SELECT ALL THE BEACONS IN THE DATABASE */
    function get_all_beacon(){
        global $dbconn;
        
        $get_beacon = "select * from beacon";
        $result = pg_query($dbconn, $get_beacon);
        
        //confirm_query($result);

        return $result;
            
    }
        
    function get_user_info($username){
        global $dbconn;

        $query = "SELECT * FROM users ";
        $query .= "WHERE username = '$username' ";

        $result = pg_query($dbconn,$query);
        confirm_query($result);

        return $result;

    }
    
    /* Check if user is logged in*/
    function logged_in(){
        return isset($_SESSION['username']);
    }
    
    /* If user is NOT logged in, the system redirects to 
     the index for login page */
    function confirm_logged_in(){
        if (!logged_in()){
            redirect_to("index.php");
        }
    }

    /* Select all the rooms */    
    function get_all_room_info(){
        
        $get_room = "select * from rooms";
        $fetch_room = pg_query($dbconn, $get_room);
     
    }
    
    /* Show lesson per Teacher, to get their timetable */
    function get_lesson_per_user($user_id){
        global $dbconn;

        $query = "SELECT * FROM lesson ";
        $query .= "where lesson_teacher = '$user_id' ";

        $fetch_lesson_user = pg_query($dbconn, $query);
        

        return $fetch_lesson_user;
    }
    
    /*  Get Timetable for the teachers */
    function get_lesson_per_user_timeslot($user_id, $lesson_date){
        global $dbconn;
        
        
        $query  = "select ls.lesson_id, cs.course_id, ls.class_id, ls.lesson_date, ls.lesson_timeslot, ts.dayofweek, rm.name, ts.start_time, ts.end_time ";
        $query .= "from lesson ls ";
        $query .= "inner join timeslots ts ";
        $query .= "on ls.lesson_timeslot = ts.slot_id ";
        $query .= "inner join class cs ";
        $query .= "on ls.class_id = cs.class_id ";
        $query .= "inner join rooms rm ";
        $query .= "on ls.lesson_room = rm.room_id and ";
        $query .= "ls.lesson_teacher = '$user_id' and ";
        $query .= "ls.lesson_date = '$lesson_date'  ";
        $query .= "ORDER BY ts.slot_id ASC ";
        
        $fetch_lesson_user_timeslot = pg_query($dbconn, $query);
        
        return $fetch_lesson_user_timeslot;
        
    }
    
    function get_timetable_per_user($lesson_id){
        global $dbconn;
        
        $query  = "select ls.lesson_id, ls.class_id, ls.lesson_date, ls.lesson_timeslot, ts.dayofweek, rm.name, ts.start_time, ts.end_time ";
        $query .= "from lesson ls ";
        $query .= "inner join timeslots ts ";
        $query .= "on ls.lesson_timeslot = ts.slot_id ";
        $query .= "inner join rooms rm ";
        $query .= "on ls.lesson_room = rm.room_id and ";
        $query .= "ls.lesson_id = '$lesson_id' and ";
        $query .= "ORDER BY ls.lesson_date DESC ";
        
        $fetch_timetable_per_user = pg_query($dbconn, $query);
        
        return $fetch_timetable_per_user;
        
    }
    
    function get_timetable_per_user_dayofweek($user_id, $lesson_date){
        global $dbconn;
        
        
        $query  = "select ls.lesson_id, ls.class_id, ls.lesson_date, ls.lesson_timeslot, ts.dayofweek, rm.name, ts.start_time, ts.end_time ";
        $query .= "from lesson ls ";
        $query .= "inner join timeslots ts ";
        $query .= "on ls.lesson_timeslot = ts.slot_id ";
        $query .= "inner join rooms rm ";
        $query .= "on ls.lesson_room = rm.room_id and ";
        $query .= "ls.lesson_teacher = '$user_id' and ";
        $query .= "ls.dayofweek = '$lesson_date' ";
        $query .= "ORDER BY ls.lesson_date DESC ";
        
        $fetch_timetable_per_user = pg_query($dbconn, $query);
        
        return $fetch_timetable_per_user;
        
    }
    
    /* Show table for the attendance */
    function get_lesson_per_student($user_id, $lesson_id){
        global $dbconn;
        
        $query = "Select st.student_fname, st.student_lname, st.student_prefname, st.stream, cl.start_date, cl.end_date, cl.class_id, en.end_date, en.enrol_date, en.commenced, ls.lesson_date, ls.lesson_id, ts.dayofweek, ts.start_time, ts.end_time, st.student_id, rm.name ";
        $query .= "from student st ";
        $query .= "inner join enrol en ";
        $query .= "on en.student_id = st.student_id ";
        $query .= "inner join class cl ";
        $query .= "on en.class_id = cl.class_id ";
        $query .= "inner join lesson ls ";
        $query .= "on ls.class_id = cl.class_id ";
        $query .= "inner join rooms rm ";
        $query .= "on rm.room_id = ls.lesson_room ";
        $query .= "inner join timeslots ts ";
        $query .= "on ts.slot_id = ls.lesson_timeslot and ";
        $query .= " ls.lesson_teacher = '$user_id' and ";
        $query .= " ls.lesson_id = '$lesson_id' ";
        
        $fetch_student_per_lesson = pg_query($dbconn, $query);
        
        confirm_query($fetch_student_per_lesson);
        
        return $fetch_student_per_lesson;
        
    }
    
    /* Check in the database if there is any record of attendance
    depending of the student_id and lesson_id */
    function get_attendance($student_id, $lesson_id){
        global $dbconn;
        
        $query = "select * from attend ";
        $query .= "where student_id = '$student_id' and ";
        $query .= "lesson_id = '$lesson_id' ";
        
        $fetch_attendance_per_student_lesson = pg_query($dbconn, $query);

        confirm_query($fetch_attendance_per_student_lesson);
        
        return $fetch_attendance_per_student_lesson;
        
    }
    
    function check_if_marked($attend_id){
        global $dbconn;
        
        $query = "select * from attend ";
        $query .= "where attend_id = '$attend_id' ";
        
        $fetch_attend = pg_query($dbconn, $query);

        confirm_query($fetch_attend);
        
        $result_row_count = pg_num_rows($fetch_attend);
        
        return $result_row_count;
        
        
    }
    
    
    function mark_attendance($student_id, $student_lesson, $lesson_hours, $late_check){
        global $dbconn;
        
        $query = "INSERT INTO attend (student_id, lesson_id, hours_attend, late) ";
        $query .= " VALUES ('".$student_id."', '".$student_lesson."', '".$lesson_hours."', '".$late_check."') ";

        
        $result_set = pg_query($dbconn, $query);
        confirm_query($result_set);
        
        return $result_set;
            
    }
    
    function delete_attendance($student_id, $lesson_id){
        global $dbconn;
        
        $query =  "DELETE from attend where lesson_id = '$lesson_id' ";
        $query .= "and student_id = '$student_id' ";
        
        $result_set = pg_query($dbconn, $query);
        confirm_query($result_set);
        
        return $result_set;
        
    }
    
    function get_lesson_date($user_id){
        global $dbconn;

        $query = "SELECT DISTINCT lesson_date FROM lesson ";
        $query .= "where lesson_teacher = '$user_id' ";
        $query .= "ORDER BY lesson_date DESC ";
        

        $lesson_date_set = pg_query($dbconn, $query);
        confirm_query($lesson_date_set);

        return $lesson_date_set;

    }
    
    function get_attendance_detail($student_id, $class_id){
        global $dbconn;
        
        
        $query = "Select st.student_fname, st.student_lname, st.student_prefname, st.stream, cl.start_date, cl.end_date, en.end_date, en.enrol_date, en.commenced, ls.lesson_date, ls.lesson_id, st.student_id ";
        $query .= "from lesson ls ";
        $query .= "inner join class cl ";
        $query .= "on cl.class_id = ls.class_id ";
        $query .= "inner join enrol en ";
        $query .= "on en.class_id = cl.class_id ";
        $query .= "inner join student st ";
        $query .= "on st.student_id = en.student_id and ";
        $query .= "st.student_id = '$student_id' and ";
        $query .= "cl.class_id = '$class_id' ";
//        $query .= "ls.lesson_date <= '$today_date' ";
        
        $fetch_attendance_detail = pg_query($dbconn, $query);
        
        return $fetch_attendance_detail;
        
    }
    
    function check_attendance_student_lesson($student_id, $lesson_id){
        global $dbconn;
        
        $query = "select attend_id from attend ";
        $query .= "where lesson_id = '$lesson_id' and ";
        $query .= "student_id = '$student_id' ";
        
        $fetch_attend = pg_query($dbconn, $query);

        confirm_query($fetch_attend);
        
        return $fetch_attend;
        
    }
    
    function number_of_lessons($user_id, $today){
        
        $fetch_timetable_per_user = get_lesson_per_user_timeslot($user_id, $today);
        
        $count_lessons_in_the_day = pg_num_rows($fetch_timetable_per_user);
        
        return $count_lessons_in_the_day;
        
        
    }
    
  
?>