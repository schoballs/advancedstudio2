<?php 
    require_once('web-interface/includes/session.php');
    
?>

<?php
    
    require_once("../../connection/config.php");
    require_once("../functions/functions.php");

    $submit = filter_input(INPUT_POST, "submit");
    if($submit){

        $room_id = pg_escape_string($_POST['room_id']);
        $beacon_serial_nu = pg_escape_string($_POST['beacon_serial_nu']);
        $beacon_uuid = pg_escape_string($_POST['beacon_uuid']);
        $beacon_region = pg_escape_string($_POST['beacon_region']);
        $beacon_major = pg_escape_string($_POST['beacon_major']);
        $beacon_minor = pg_escape_string($_POST['beacon_minor']);

        //echo $beacon_major." ".$beacon_uuid." ".$beacon_region." ".$beacon_serial_nu." ".$beacon_minor." ".$room_id;

        //validate if the beacon exists.
        $result = get_beacon_info($beacon_uuid);
        
        //how many rows where returned
        $result_row_count = pg_num_rows($result);
        
        //no rows returned therefore the user doesn't exist
        //echo $result_row_count;
        
        if($result_row_count == 0){

            register_beacon($room_id, $beacon_serial_nu, $beacon_uuid, $beacon_region, $beacon_major, $beacon_minor);
            
            $_SESSION['message_success'] ="Beacon ".$beacon_uuid." for the room ".$room_id." was created successfully! ";
            
            header("Location: ../../../body-website.php?get-beacon");
        }else{
            //beacon already exists
            $_SESSION['message_danger'] ="The Beacon with the UUID ".$beacon_uuid." already exists";
            header("Location: ../../../body-website.php?add-beacon");
            
        }
        // Release result sets
        mysqli_free_result($result);
        if(isset($dbconn)){
            mysqli_close($dbconn);
        }
    }
    
   
        
   

