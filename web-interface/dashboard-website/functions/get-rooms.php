<?php 
    require_once('web-interface/includes/session.php');
    include("web-interface/connection/config.php");
    require_once("web-interface/dashboard-website/functions/functions.php");
?>

<h2 class="sub-header">Rooms List</h2>            

<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Room Id</th>
                <th>Name</th>
                <th>Description</th>
                <th>Capacity</th>
                <th>Sort</th>
                <th>Removed</th>
            </tr>
        </thead>

        <tbody>
             <?php

                $fetch_room = get_rooms();

                $index = 0;

                while($room_row = pg_fetch_array($fetch_room)){

                    $room_id = $room_row['room_id'];   
                    $room_name = $room_row['name']; 
                    $room_description = $room_row['description']; 
                    $room_capacity = $room_row['capacity']; 
                    $room_sort = $room_row['sort'];
                    $room_removed = $room_row['removed']; 

                    $index++;

            ?>

            <tr align="center">
                <td><?php echo $room_id; ?></td>
                <td><?php echo $room_name; ?></td>
                <td><?php echo $room_description; ?></td>
                <td><?php echo $room_capacity; ?></td> 
                <td><?php echo $room_sort; ?></td> 
                <td><?php echo $room_removed; ?></td>
            </tr>

            <?php } ?>

        </tbody>
    </table>
</div>            