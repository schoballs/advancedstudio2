<?php 
    require_once('web-interface/includes/session.php');
?>

<?php 
    include("web-interface/connection/config.php");
    require_once("web-interface/dashboard-website/functions/functions.php");
?>

<div class="container col-lg-7 col-md-7 col-sm-5 center-page" >
    <form action="web-interface/dashboard-website/functions/add-beacon-action.php" id="login-action" method="post">
        <div id="form-group">
            <fieldset>
                <legend>Add Beacon Device</legend>
                <div id="form-group">
                    <select type="" class="form-control" id="room_id" name="room_id" placeholder="Room" hint="Room Name">
                        <option value="" disabled selected hidden>Choose room...</option>
                        <?php  $room_set = get_rooms(); ?>

                        <?php 
                            while($room_row = pg_fetch_array($room_set)){
                        ?>        
                           <option value="<?php echo $room_row['room_id'];?>"> <?php echo $room_row['name']; ?> </option>    
                        <?php        
                            }
                        ?>
                    </select>     
                    <br>
                </div>
                <div id="form-group">
                    <input type="text" class="form-control" id="beacon_serial_nu" name="beacon_serial_nu" placeholder="Beacon Serial Number" autofocus /><br>
                </div>
                <div id="form-group">
                    <input type="number" class="form-control" id="beacon_uuid" name="beacon_uuid" placeholder="Beacon UUID"  /><br>
                </div>
                <div id="form-group">
                    <input type="text" class="form-control" id="beacon_region" name="beacon_region" placeholder="Beacon Region"/><br>
                </div>
                <div id="form-group">
                    <input type="number" class="form-control" id="beacon_major" name="beacon_major" placeholder="Beacon Major"  /><br>
                </div>
                <div id="form-group">
                    <input type="number" class="form-control" id="beacon_minor" name="beacon_minor" placeholder="Beacon Minor"  /><br>
                </div>
                <div id="form-group">
                    <input type="submit" class="btn btn-primary" id="submit" name="submit" value="Add Beacon">
                </div>
            </fieldset>
        </div>
    </form>
</div>


