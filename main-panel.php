    <?php require_once('web-interface/includes/session.php'); ?>
    
    <?php //confirm_logged_in(); ?>
    
                <?php
                    $page = filter_input(INPUT_GET, "page");
                    if(!$page){
                     $page = "NULL";   
                    }
                ?>
                
                <?php 
                    // if the user press the button from the side bar menu 
                    // the panel is loaded with the information called
                    
                    if(isset($_GET['dashboard'])){
                    
                       include("web-interface/dashboard-website/functions/dashboard.php");
                    }   
                    
                    if(isset($_GET['timetable'])){

                        include("web-interface/dashboard-website/functions/get-lesson-per-user.php");
                    }
                    
                    if(isset($_GET['mark-attendance'])){

                        include("web-interface/dashboard-website/functions/mark-attendance.php");
                    }
                    
                    if(isset($_GET['student_attend_detail'])){

                        include("web-interface/dashboard-website/functions/student_attend_detail.php");
                    }
                 ?>
            
    <?php 
    
        if(isset($_GET['add-beacon'])){
           include("web-interface/dashboard-website/forms/add-beacon.php");
        }
    
    
    ?>