


<footer class="footer navbar-fixed-bottom ">
    
<!--    //Developed by The Interactive Hippo and Designed by Chiaki Iwamoto.-->
    
</footer>
<?php

if (isset($dbconn)){
        pg_close($dbconn);
        
}
?>

<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="web-interface/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="web-interface/assets/js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="web-interface/assets/js/ie10-viewport-bug-workaround.js"></script>
    <!-- Latest js code -->
    <script  type="text/javascript" src="web-interface/js/jquery-1.12.0.js"></script>
    <!-- Tablesorter js! -->
    <script  type="text/javascript" src="web-interface/Mottie-tablesorter-08bf513/js/jquery.tablesorter.js"></script>
    <!-- Jquery version for the side bar menu-->
    <script src="web-interface/js/jquery-1.11.3.js"></script>
    <!-- Javascript code for the sidebar menu-->
    <script src="web-interface/js/simple-sidebar.js"></script>
    
  </body>
</html>