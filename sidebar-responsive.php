<?php 
    require_once('web-interface/includes/session.php');
?>

    
    <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu">
                
                
                <!-- ROLE CONTROL OVER THE MENUS-->
                <?php 
                    if($_SESSION['role_id'] == '1'){
                ?>
                <!-- MENU TO BE SHOWN ACCORDING TO THE ROLE OF THE USER LOGGED IN-->
                
                <li>
                    <a href="body-website.php?dashboard"> <span class="fa-stack fa-lg pull-left"><i class="fa fa-dashboard fa-stack-1x "></i></span>Dashboard</a>
                </li>
                 <li>
                    <a href="body-website.php?timetable"><span class="fa-stack fa-lg pull-left"><i class="fa fa-calendar fa-stack-1x "></i></span>Timetable</a>
                </li>
                <li>
                    <a href="body-website.php?mark-attendance"><span class="fa-stack fa-lg pull-left"><i class="fa fa-check-square-o fa-stack-1x "></i></span>Mark Attendance</a>
                </li>
                
                <?php
                    }
                ?>
                
                <!-- ROLE CONTROL OVER THE MENUS-->
                <?php 
                    if($_SESSION['role_id'] == 'student'){
                ?>
                <!-- MENU TO BE SHOWN ACCORDING TO THE ROLE OF THE USER LOGGED IN-->
                <li>
                    <a href="body-website-student.php?<?php echo ($session_login == 'YES' ? 'login='.urlencode("YES").'&' : ''); ?>get-rooms"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span>Rooms</a>
                </li>
                <li>
                    <a href="body-website-student.php?<?php echo ($session_login == 'YES' ? 'login='.urlencode("YES").'&' : ''); ?>get-timeslots"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span>Timeslots</a>
                </li>
                <li>
                    <a href="body-website-student.php?<?php echo ($session_login == 'YES' ? 'login='.urlencode("YES").'&' : ''); ?>get-student"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span>Student</a>
                </li>
                <li>
                    <a href="body-website-student.php?<?php echo ($session_login == 'YES' ? 'login='.urlencode("YES").'&' : ''); ?>get-class"><span class="fa-stack fa-lg pull-left"><i class="fa fa-youtube-play fa-stack-1x "></i></span>Class</a>
                </li>
                 <li>
                    <a href="body-website-student.php?get-lesson-per-user"><span class="fa-stack fa-lg pull-left"><i class="fa fa-cloud-download fa-stack-1x "></i></span>Timetable</a>
                </li>
                
                <?php
                    }
                ?>
                
                <!-- ROLE CONTROL OVER THE MENUS-->
                <?php 
                    if($_SESSION['role_id'] == '3'){
                ?>
                <!-- MENU TO BE SHOWN ACCORDING TO THE ROLE OF THE USER LOGGED IN-->
                <li>
                    <a href="body-website.php?get-enrol"><span class="fa-stack fa-lg pull-left"><i class="fa fa-cloud-download fa-stack-1x "></i></span>Enrol</a>
                </li>
                <li>
                    <a href="body-website.php?<?php echo ($session_login == 'YES' ? 'login='.urlencode("YES").'&' : ''); ?>get-beacon"><span class="fa-stack fa-lg pull-left"><i class="fa fa-cloud-download fa-stack-1x "></i></span>Beacon</a>
                </li>
                <li>
                    <a href="body-website.php?<?php echo ($session_login == 'YES' ? 'login='.urlencode("YES").'&' : ''); ?>add-beacon"><span class="fa-stack fa-lg pull-left"><i class="fa fa-cloud-download fa-stack-1x "></i></span>Add Beacon</a>
                </li>
                <li>
                    <a href="body-website.php?get-lesson-per-user"><span class="fa-stack fa-lg pull-left"><i class="fa fa-cloud-download fa-stack-1x "></i></span>Timetable</a>
                </li>
                
                <li>
                    <a href="body-website.php?get-lesson"><span class="fa-stack fa-lg pull-left"><i class="fa fa-youtube-play fa-stack-1x "></i></span>Lessons</a>
                </li>
                
                <li>
                    <a href="body-website.php?get-attend"><span class="fa-stack fa-lg pull-left"><i class="fa fa-cloud-download fa-stack-1x "></i></span>Attendance</a>
                </li>
                
                <?php
                    }
                ?>
                
        </ul>    
                
                <!-- MENUS with dropdown options
                <li class="active">
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-dashboard fa-stack-1x "></i></span> Dashboard</a>
                       <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="#">link1</a></li>
                        <li><a href="#">link2</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-flag fa-stack-1x "></i></span> Shortcut</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-flag fa-stack-1x "></i></span>link1</a></li>
                        <li><a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-flag fa-stack-1x "></i></span>link2</a></li>
                    </ul>
                </li>-->
                
            
        </div><!-- /#sidebar-wrapper -->
        
    </div>
