<?php  
    require_once('web-interface/includes/session.php');
    
?>

<div class="row">
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-pink">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-users fa-4x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge-user"> Student</div>
                    </div>
                </div>
            </div>
            <a href="body-website-student.php">
                <div class="panel-footer">
                    <span class="pull-left">Login</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-user fa-4x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge-user"> Teacher</div>
                    </div>
                    
                </div>
            </div>
            <a href="body-website.php">
                <div class="panel-footer">
                    <span class="pull-left">Login</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
</div>

