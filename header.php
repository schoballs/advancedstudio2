<?php require_once('web-interface/includes/session.php'); ?>


<!DOCTYPE html>
<html lang="en">
  <head>
  
		
  
  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../favicon.ico">

    <title>AITendance - Automated System of Attendance Control</title>

    <!-- Bootstrap core CSS -->
    <link href="web-interface/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="web-interface/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    
    <!-- Custom styles for this kind of dashboard -->
    <link href="web-interface/css/dashboard.css" rel="stylesheet">
    <link href="web-interface/css/responsive-sidebar.css" rel="stylesheet">
    <link href="web-interface/css/custom.css" rel="stylesheet">
    <link href="web-interface/font-awesome-4.5.0/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="web-interface/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="web-interface/assets/js/ie-emulation-modes-warning.js"></script>
    <!--<link href="web-interface/Mottie-tablesorter-08bf513/docs/css/jq.css" rel="stylesheet">-->
    <!-- choose a theme file -->
    <link rel="stylesheet" href="web-interface/Mottie-tablesorter-08bf513/css/theme.default.css">
    <!-- load jQuery and tablesorter scripts -->
    <script type="text/javascript" src="web-interface/Mottie-tablesorter-08bf513/docs/js/jquery-latest.min.js"></script>
    <script type="text/javascript" src="web-interface/Mottie-tablesorter-08bf513/js/jquery.tablesorter.js"></script>

    <!-- tablesorter widgets (optional) -->
    <script type="text/javascript" src="web-interface/Mottie-tablesorter-08bf513/js/jquery.tablesorter.widgets.js"></script>
    <script>
        $(function(){
            $("#myTable").tablesorter();
        });
    </script>    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
      
        <?php

             //validate the $_SESSION because, someone could add to the url :  ?login=YES, when is not logged in
            if(!$session_login = filter_var($_SESSION["login"])){
                //the $_SESSION["login"] is not set, therefore there is no user logged in
                //echo "user is not loged in 2";
                $_SESSION["login"] = "NO";
                session_destroy();
            }else{
                //echo "<pre>" .print_r($_SESSION, TRUE). "</pre>";
                if($session_login == "YES"){
                    //the user is loged in
                    echo "";
                }
            }
            
        ?>
      
      

    <?php
        
        include 'web-interface/connection/config.php';
        
        // top bar of the dashboard
        include 'top-bar.php';
        
        
    ?>
      
      
    <div class="container-fluid xyz">
        <div class="row rowTop">   
            <?php 
                // left menu bar
                include 'sidebar-responsive.php';
            ?> 
