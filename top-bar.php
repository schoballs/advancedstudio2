<?php require_once('web-interface/includes/session.php'); ?>

    <nav class="navbar navbar-default navbar-fixed-top">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header fixed-brand">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
            </button>

            <a class="navbar-brand" href="index.php"><img src="web-interface/images/logo.png" id="logo"/></a>
            </div><!-- navbar-header-->

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active" >
                    <button class="navbar-toggle collapse in" data-toggle="collapse" id="menu-toggle-2"> 
                        <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                    </button>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                
                <?php 
                        
                    if($session_login == "YES"){ ?>
                        <li>
                            <a> 
                                <span class="glyphicon glyphicon-time"> 
                                    <?php 
                                    date_default_timezone_set("Australia/Sydney");
                                    echo date("Y-m-d h:i:sa") . "<br>"; 
                                    ?>
                                </span>
                            </a>    
                        </li>
                        <li><a> <span class="welcome"></span>Welcome, <?php echo $_SESSION['user_fname']; ?></a></li>
                        <li><?php
                            if($_SESSION['role_id'] == 'student'){
                                echo '<a href="web-interface/dashboard-website-student/functions/logout.php"><span class="glyphicon glyphicon-log-out"></span>Logout</a></li>';
                            }else{
                                echo '<a href="web-interface/dashboard-website/functions/logout.php"><span class="glyphicon glyphicon-log-out"></span>Logout</a></li>';
                            }
                           
                    }else{ ?>
                        
                    <?php
                    }
                    
                ?>
            </ul>
        </div>
            
        
        <!-- bs-example-navbar-collapse-1 -->
    </nav>
